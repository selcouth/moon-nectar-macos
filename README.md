# Moon Nectar MacOS Plugins

MacOS Specific plugins for the Moon Nectar Media Player.

Plugins provide the following functionality:

* Prevent system sleep while media is playing
* Media key support (play/pause, next, previous)

