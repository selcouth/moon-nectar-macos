import pathlib
import setuptools
import distutils
import os
import subprocess
import re

entry_points = [
    "Registry.Control.MacOSNoSleep        = mnmacos.macos_nosleep:MacOSNoSleep",
    "Registry.Control.MacNowPlayingPlugin = mnmacos.macos_now_playing:MacNowPlayingPlugin",
]

# Get the app version
# ... By switching to the package directory, the version file becomes a local import
# ... This avoids problems with missing dependencies
appvars = {}
with open("mnmacos/vars.py") as fp:
    exec(fp.read(), appvars)


module_packages = setuptools.find_packages()

def read_text(filename: str):
    return open(filename).read()

setuptools.setup(
    name                 = "mnmacos",
    packages             = module_packages,
    use_scm_version      = True,
    license              = read_text("LICENSE.txt"),
    description          = "Moon Nectar Media Player MacOS Plugins",
    author               = "David Morris",
    author_email         = "othalan@othalan.net",
    url                  = "https://gitlab.com/othalan/moon-nectar-macos",
    include_package_data = True,
    zip_safe             = False,
    python_requires      = '>=3.8',
    entry_points         = {
        "mnectar.plugins": entry_points,
    },
    classifiers = [
        # complete classifier list: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
        "Operating System :: MacOS :: MacOS X",
        "Natural Language :: English",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.8",
        "Topic :: Multimedia :: Sound/Audio :: Players",
    ],
    setup_requires = [
        'setuptools_scm'
    ],
    install_requires = [
        "mnectar>=0.9.0",
        "PluginRegistry>=0.3.0",
        "pyobjc; sys_platform == 'darwin'",
    ],
    extras_require = {
        "test": [
            "pyqt6",
            "coveralls",
            "pytest",
            "pytest-clarity",
            "pytest-cov",
            "pytest-mock",
            "pytest-qt",
            "pytest-sugar",
            "pyfakefs",
        ],
    },
)
