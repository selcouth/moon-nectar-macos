import logging

_logger = logging.getLogger("mnectar."+__name__)

from mnectar.vars import __appname__
from mnectar.registry import Registry, Plugin
from mnectar.util.decorator import oscheck


class MacOSNoSleep(Plugin, registry=Registry.Control):
    """
    Request the MacOS not enter sleep mode because of idle time while music is playing.

    Does nothing for other methods of sleep (such as user requested)
    """

    ############################################################################
    #
    # Preventing MacOS from sleeping must go through the operating system function calls
    # to the IOKit Framework.  This can be a bit challenging from python as the process
    # is poorly documented and involves various bugs.  Note that there are numerous other
    # implementations available, however they are generally more complex, more difficult
    # to maintain, and/or require working around bugs in the MacOS operating system.
    #
    # Relevant documentation:
    #
    # Bridge Support Documentation:
    # https://opensource.apple.com/source/pyobjc/pyobjc-47/pyobjc/pyobjc-core-2.5.1/Doc/metadata/bridgesupport.rst
    #
    # PyObjC documentation for loadBundleFunctions:
    # https://pyobjc.readthedocs.io/en/latest/metadata/manual.html?highlight=loadbundlefunctions
    #
    # PyObjC output function parameters passed by reference:
    # https://pyobjc.readthedocs.io/en/latest/dev/wrapping.html
    #
    # Various examples of how to use loadBundleFunctions:
    # https://www.programcreek.com/python/example/99193/objc.loadBundleFunctions
    #
    # Objective C type encodings used in loadBundleFunctions
    # https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtTypeEncodings.html
    #
    # Blog listing how to use bridge support files (not used in this implementation):
    # http://michaellynn.github.io/2015/08/08/learn-you-a-better-pyobjc-bridgesupport-signature/
    #
    # Apple Q&A 1340: How can my application get notified when the computer is going to
    # sleep or waking from sleep? How to I prevent sleep?
    # ... this is objective c code used as a starting point to using PyObjC.
    # https://developer.apple.com/library/archive/qa/qa1340/_index.html
    #
    ############################################################################

    # Define constants used by the IOKit Framework
    # Source: /System/Library/Frameworks/IOKit.framework/Versions/A/Resources/BridgeSupport/IOKit.bridgesupport
    # ... As of 2019/09/29, there is no easy way to import these definitions directly
    #     via objc or the bridge support file.  Rather than hack together a complex
    #     solution, the values are instead hard coded as they are unlikely to change.
    kIOPMAssertionTypeNoIdleSleep = "NoIdleSleepAssertion"
    kIOPMAssertionLevelOn = 255

    # Function definitions (prototypes) from IOKit
    # Source: /System/Library/Frameworks/IOKit.framework/Versions/A/Resources/BridgeSupport/IOKit.bridgesupport
    #
    # Format: A list of tuples: (function, argument_definition)
    #
    # NOTE: The function argument definition starts with the return value in the first
    #       letter.
    # NOTE: The function argument definition must be a byte string, which is not listed
    #       in most information around the web (recent change as of 2019/09/29?)
    FUNCTIONS = [
        ("IOPMAssertionCreateWithName", b"i^{__CFString=}I^{__CFString=}o^I"),
        ("IOPMAssertionRelease", b"iI"),
    ]

    # Reason idle sleep is being prevented.
    # ... this is for viewing by other applications and the user
    # ... view status using this command:
    #     pmset -g assertions
    REASON = f"{__appname__} plugin {__name__}: Disable idle sleep while music is playing."

    # The "assert id" tracks the MacOS internal pointer to the idle sleep disable request
    # and is needed to cancel the feature.  A value of '0' is used to indicate the
    # feature is turned off.
    assert_id = 0

    def __init__(self, *arg, **kw):
        super().__init__(*arg, **kw)


    @oscheck(target="Darwin")
    def enable(self):
        import objc
        from Foundation import NSBundle

        IOKit = NSBundle.bundleWithIdentifier_("com.apple.framework.IOKit")

        objc.loadBundleFunctions(IOKit, globals(), self.FUNCTIONS)

        self.app.signal.playing.connect(self.on_playing)
        self.app.signal.paused.connect(self.on_stopped_paused)
        self.app.signal.stopped.connect(self.on_stopped_paused)

        _logger.debug("MacOSNoSleep: Enabled")

    @oscheck(target="Darwin")
    def on_playing(self, *arg):
        if self.assert_id == 0:
            # Prevent MacOS idle sleep
            errcode, assert_id = IOPMAssertionCreateWithName(
                self.kIOPMAssertionTypeNoIdleSleep,
                self.kIOPMAssertionLevelOn,
                self.REASON,
                None,
        )

            if errcode:
                _logger.error(f"Failed to disable idle sleep with error code: {assert_id}")
            else:
                self.assert_id = assert_id

    @oscheck(target="Darwin")
    def on_stopped_paused(self):
        if self.assert_id != 0:
            # Permit MacOS Idle Sleep
            errcode = IOPMAssertionRelease(self.assert_id)
            self.assert_id = 0

    @oscheck(target="Darwin")
    def disable(self):
        self.app.signal.playing.disconnect(self.on_playing)
        self.app.signal.paused.disconnect(self.on_stopped_paused)
        self.app.signal.stopped.disconnect(self.on_stopped_paused)

        _logger.debug("MacOSNoSleep: Disabled")
