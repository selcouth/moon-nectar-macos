import logging

import mnectar

from mnectar import logConfig  # Import first for proper log file setup

# Import version number generated from setuptools_scm
from importlib.metadata import version, PackageNotFoundError

try:
    __version__ = version(__name__)
except PackageNotFoundError:
    # package is not installed
   pass
