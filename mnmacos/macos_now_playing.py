# MacOS Now Playing interaction
# Development notes: https://gitlab.com/selcouth/moon-nectar-macos/-/issues/1

import logging

from AppKit import NSImage
from AppKit import NSMakeRect
from AppKit import NSCompositingOperationSourceOver
from Foundation import NSMutableDictionary
from MediaPlayer import MPNowPlayingInfoCenter
from MediaPlayer import MPRemoteCommandCenter
from MediaPlayer import MPMediaItemArtwork
from MediaPlayer import MPMediaItemPropertyTitle
from MediaPlayer import MPMediaItemPropertyArtist
from MediaPlayer import MPMediaItemPropertyPlaybackDuration
from MediaPlayer import MPMediaItemPropertyArtwork
from MediaPlayer import MPMusicPlaybackState
from MediaPlayer import MPMusicPlaybackStatePlaying
from MediaPlayer import MPMusicPlaybackStatePaused

from mnectar.registry       import Plugin, Registry
from mnectar.registry       import PluginSetting
from mnectar.util.signal    import Signal
from mnectar.util.decorator import oscheck

class MacNowPlayingPlugin(Plugin, registry=Registry.Control):
    def __init__(self, *arg, **kw):
        super().__init__(*arg, **kw)

        self._logger = logging.getLogger("mnectar."+__name__)

        self.cmd_center = MPRemoteCommandCenter.sharedCommandCenter()
        self.info_center = MPNowPlayingInfoCenter.defaultCenter()

    def hSimple(self, signal, event):
        """
        A simple input event handler which generates the specified event.
        """
        signal.emit()
        self._logger.debug(f"MacOS Signal: {signal} => {event}")
        return 0

    @oscheck(target="Darwin")
    def enable(self):
        # Enable Commands
        self.cmd_center.playCommand()           .addTargetWithHandler_(lambda evt: self.hSimple(self.app.signal.play, evt))
        self.cmd_center.pauseCommand()          .addTargetWithHandler_(lambda evt: self.hSimple(self.app.signal.pause, evt))
        self.cmd_center.togglePlayPauseCommand().addTargetWithHandler_(lambda evt: self.hSimple(self.app.signal.mmkey_togglePause, evt))
        self.cmd_center.nextTrackCommand()      .addTargetWithHandler_(lambda evt: self.hSimple(self.app.signal.mmkey_playNext, evt))
        self.cmd_center.previousTrackCommand()  .addTargetWithHandler_(lambda evt: self.hSimple(self.app.signal.mmkey_playPrev, evt))

        # Enable Status
        self.app.signal.paused .connect(self.onPaused)
        self.app.signal.playing.connect(self.onPlaying)

        return True

    @oscheck(target="Darwin")
    def disable(self):
        ...

    def onStopped(self):
        self.info_center.setPlaybackState_(MPMusicPlaybackStateStopped)
        return 0

    def onPaused(self):
        self.info_center.setPlaybackState_(MPMusicPlaybackStatePaused)
        return 0

    def onPlaying(self, ptr, length):
        nowplaying_info = NSMutableDictionary.dictionary()
        nowplaying_info[MPMediaItemPropertyTitle] = ptr.record['title']
        nowplaying_info[MPMediaItemPropertyArtist] = ptr.record['artist']
        nowplaying_info[MPMediaItemPropertyPlaybackDuration] = length

        cover = ptr.record.cover
        if cover is not None:
            # Apple documentation on how to load and set cover artwork is mostly useless
            # The below code was cobbled together from numerous sources
            # ... REF: https://stackoverflow.com/questions/11949250/how-to-resize-nsimage/17396521#17396521
            # ... REF: https://developer.apple.com/documentation/mediaplayer/mpmediaitemartwork?language=objc
            # ... REF: https://developer.apple.com/documentation/mediaplayer/mpmediaitemartwork/1649704-initwithboundssize?language=objc

            img = NSImage.alloc().initWithData_(cover)

            def resize(size):
                self._logger.debug(size)
                new = NSImage.alloc().initWithSize_(size)
                new.lockFocus()
                img.drawInRect_fromRect_operation_fraction_(
                    NSMakeRect(0, 0, size.width, size.height),
                    NSMakeRect(0, 0, img.size().width, img.size().height),
                    NSCompositingOperationSourceOver,
                    1.0,
                )
                new.unlockFocus()
                return new
            if img is not None:
                art = MPMediaItemArtwork.alloc().initWithBoundsSize_requestHandler_(img.size(), resize)
                nowplaying_info[MPMediaItemPropertyArtwork] = art
            else:
                self._logger.warning(f"Album [{ptr.record['album']}] cover art found but NSImage is 'None' for record: {ptr.record.mrl}")
                self._logger.debug(f"Cover Type [{type(cover)}]: {cover}")

        self.info_center.setNowPlayingInfo_(nowplaying_info)
        self.info_center.setPlaybackState_(MPMusicPlaybackStatePlaying)

        return 0

