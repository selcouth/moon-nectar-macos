import asyncio
import logging
import os
import pathlib
import pytest
import threading
import threading
import vlc

import _pytest.logging

from mnectar         import logConfig
from mnectar.appinit import AppInit
from mnectar.backend.mock import BackendMock

_pytest.logging.ColoredLevelFormatter.LOGLEVEL_COLOROPTS[logConfig.DEBUG2] = {'purple'}
_pytest.logging.ColoredLevelFormatter.LOGLEVEL_COLOROPTS[logConfig.DEBUG3] = {'purple'}

def pytest_sessionstart(session):
    # pytest provides its own log handler so remove our own custom log handler so that
    # duplicate log messages do not clutter up the test output on failures.
    logging.root.removeHandler(logConfig._stream_handler)

def pytest_sessionfinish(session, exitstatus):
    pass

def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._previousfailed = item


def pytest_runtest_setup(item):
    if "incremental" in item.keywords:
        previousfailed = getattr(item.parent, "_previousfailed", None)
        if previousfailed is not None:
            pytest.xfail("previous test failed (%s)" % previousfailed.name)

@pytest.fixture
def app(tmp_path):
    app = AppInit()
    app.init( args      = ['-U','None', '-b','None', '--plugins-disabled'],
              configdir = tmp_path)

    return app

@pytest.fixture
def backend_mock(app):
    app.backend = BackendMock(app)
    app.backend.enable()
    return app.backend

@pytest.fixture
def app_with_plugins(tmp_path):
    app = AppInit()
    app.init( args      = ['-U','None', '-b','Mock', '--log-level','INFO'],
              configdir = tmp_path)

    return app

@pytest.fixture(scope="session")
def libfile(tmp_path_factory):
    app = AppInit()
    app.init( args      = ['-U','None', '-b','None'],
              configdir = tmp_path_factory.mktemp("app-session"))

    # TODO: Decide on if this test fixture should use:
    #       - A pre-build database checked into the code
    #       - A scan of checked in data files
    #       - A fake album generator
    if False:
        # Scan checked in files
        #XXX Disabled at the moment because no test files are checked in
        library_file_dir = tmp_path_factory.mktemp("library-session-database")
        library_dir  = pathlib.Path(__file__).with_name('data')
        library_file = library_file_dir/'test.json'
        app.library.dbLoad(library_file)
        app.library.scanDirectory(library_dir)
    else:
        # Use a pre-built database checked in with tests
        library_file = pathlib.Path(__file__).with_name('data')/'test.json'
    del app
    yield library_file


@pytest.fixture
def slot():
    """
    Returns a context manager which acts as a slot for a signal
    The slot counts the number of times it was called
    and stores the most recent arguments received by the signal

    Example:

        >>> with slot(my_signal) as my_slot:
        ...     my_signal.emit(5)
        ...     print(my_slot.count, my_slot.args)
        1 [5]
        ...     my_signal.emit(9)
        ...     print(my_slot.count, my_slot.args)
        2 [9]

    """
    class Slot:
        def __init__(self, signal):
            self.count  = 0
            self.signal = signal
            self.signal.connect(self._callback)
            self.event = threading.Event()

        def wait(self, timeout=1.0):
            status = self.event.wait(timeout)
            self.event.clear()
            return status

        def clear(self):
            self.event.clear()
            self.args = None

        def _callback(self, *arg):
            self.count += 1
            self.args   = arg
            self.event.set()

    def slot_generator(signal):
        return Slot(signal)

    return slot_generator

@pytest.fixture
def skipGitLab(request):
    skip = request.param
    if skip and "GITLAB_CI" in os.environ:
        pytest.skip("Audio output cannot be tested on GitLab")
    yield "skipGL"

@pytest.fixture
def vlc_emulator(mocker):

    class VlcEmulator:
        _evcb               = {}
        _event_loop_running = None
        _event_thread       = None
        _length             = 60000
        _mrl                = None
        _mute_state         = False
        _paused             = False
        _playing            = False
        _position           = 0
        _resolution         = 0.1
        _running            = False
        _time               = 0

        def __init__(self):
            self._event_loop_running = threading.Event()
            self._position_lock = threading.Lock()

            mo_vlc = mocker.patch('vlc.MediaPlayer',autospec=True)
            mvlc = self.mvlc = mo_vlc.return_value
            mvlc.is_playing        .side_effect = lambda:self._playing and not self._paused
            mvlc.set_mrl           .side_effect = self._set_mrl
            mvlc.play              .side_effect = self._play
            mvlc.pause             .side_effect = self._pause
            mvlc.stop              .side_effect = self._stop
            mvlc.set_position      .side_effect = self._set_position
            mvlc.set_time          .side_effect = self._set_time
            mvlc.get_time          .side_effect = self._get_time
            mvlc.audio_set_mute    .side_effect = self._mute
            mvlc.audio_toggle_mute .side_effect = self._toggle_mute
            mvlc.get_media         .side_effect = lambda:self
            mvlc.event_manager     .side_effect = lambda:self
            mvlc.get_state         .side_effect = self._get_state
            mvlc.get_length        .side_effect = self._get_length

        def _get_length(self, ):
            # No real length is possible so at least return any value...
            return 10000

        def _thread_start(self, ):
            self._event_thread = threading.Thread(target=self._event_loop_main, daemon=True)
            self._event_loop_running.clear()
            self._event_thread.start()
            self._wait_on_event_loop()

        def _thread_stop(self, ):
            self._running = False
            self._event_thread.join()
            self._event_loop_running.clear()
            self._event_thread = None

        def _wait_on_event_loop(self, timeout=1.0):
            running = self._event_loop_running.wait(timeout)

        def _event_loop_main(self, ):
            if self._running:
                self._logger.error("Attempt to start an event loop that is already running!")
            else:
                self._running = True
                asyncio.run(self._playing_timer())

        async def _playing_timer(self, ):
            self._event_loop_running.set()

            while self._running:
                with self._position_lock:
                    if self._playing and self._position == 0:
                        self._evcb[vlc.EventType.MediaPlayerPlaying](vlc.Event(vlc.EventType.MediaPlayerPlaying))

                        e=vlc.Event(vlc.EventType.MediaPlayerTimeChanged)
                        e.u.new_time = 0
                        self._evcb[vlc.EventType.MediaPlayerTimeChanged](e)

                        e=vlc.Event(vlc.EventType.MediaPlayerPositionChanged)
                        e.u.new_position = 0
                        self._evcb[vlc.EventType.MediaPlayerPositionChanged](e)
                        await asyncio.sleep(self._resolution)

                    if self._playing and not self._paused:
                        elapsed   = (self._resolution * 1000)
                        self._time     = int(min(self._length, self._time + elapsed))
                        self._position = min(1.0, (self._time+elapsed)/self._length)

                        e=vlc.Event(vlc.EventType.MediaPlayerTimeChanged)
                        e.u.new_time = int(self._time)
                        self._evcb[vlc.EventType.MediaPlayerTimeChanged](e)

                        e=vlc.Event(vlc.EventType.MediaPlayerPositionChanged)
                        e.u.new_position = self._position
                        self._evcb[vlc.EventType.MediaPlayerPositionChanged](e)

                        if self._position >= 1.0:
                            self._evcb[vlc.EventType.MediaPlayerEndReached](vlc.Event(vlc.EventType.MediaPlayerEndReached))
                await asyncio.sleep(self._resolution)

        def _get_time(self):
            return self._time

        def _set_time(self, timeval):
            with self._position_lock:
                self._time     = int(max(0, min(self._length, timeval)))
                self._position = max(0.0, min(1.0, timeval / self._length))

        def _pause(self, ):
            self._paused = True
            self._evcb[vlc.EventType.MediaPlayerPaused](vlc.Event(vlc.EventType.MediaPlayerPaused))

        def _play(self, ):
            if self._mrl and not self._playing:
                self._paused  = False
                self._playing = True
                self._evcb[vlc.EventType.MediaPlayerPlaying](vlc.Event(vlc.EventType.MediaPlayerPlaying))
            elif self._mrl and self._playing and self._paused:
                self._paused  = False
                self._evcb[vlc.EventType.MediaPlayerPlaying](vlc.Event(vlc.EventType.MediaPlayerPlaying))
            else:
                # Nothing to do
                pass

        def _play_mrl(self, mrl):
            self._playing  = False
            self._position = 0
            self._time     = 0
            self._mrl      = mrl
            self._play()

        def _stop(self, ):
            self._playing  = False
            self._position = 0
            self._time     = 0
            self._evcb[vlc.EventType.MediaPlayerStopped](vlc.Event(vlc.EventType.MediaPlayerStopped))

        def _togglePause(self, ):
            if self._paused:
                self._play()
            elif self._playing:
                self._pause()
            elif self._mrl:
                self._play()

        def _mute(self, state):
            if state != self._mute_state:
                if state:
                    self._evcb[vlc.EventType.MediaPlayerMuted](vlc.Event(vlc.EventType.MediaPlayerMuted))
                else:
                    self._evcb[vlc.EventType.MediaPlayerUnmuted](vlc.Event(vlc.EventType.MediaPlayerUnmuted))
            self._mute_state = state

        def _toggle_mute(self, ):
            self._mute(not self._mute_state)

        def _set_mrl(self, mrl):
            self._playing  = False
            self._paused   = False
            self._time     = 0
            self._position = 0
            self._mrl      = mrl
            self._evcb[vlc.EventType.MediaPlayerMediaChanged](vlc.Event(vlc.EventType.MediaPlayerMediaChanged))

        def _set_position(self, position):
            with self._position_lock:
                self._time     = position * self._length
                self._position = position

        def get_mrl(self):
            return self._mrl

        def event_attach(self, etype,cb):
            self._evcb[etype] = cb

        def _get_state(self):
            if self._paused:
                state = vlc.State.Paused
            elif self._playing:
                state = vlc.State.Playing
            elif not self._playing and self._position >= 1.0:
                state = vlc.State.Ended
            else:
                state = vlc.State.Stopped
            return state

    return VlcEmulator


@pytest.fixture
def vlcmock(vlc_emulator):
    emulator = vlc_emulator()
    emulator._thread_start()

    yield emulator.mvlc

    emulator._thread_stop()


@pytest.fixture
def vlcmockif(request,vlc_emulator):
    enable = request.param

    if enable:
        emulator = vlc_emulator()
        emulator._thread_start()

        yield emulator.mvlc

        emulator._thread_stop()
    else:
        yield "noEM"


@pytest.fixture
def library_factory():
    from mnectar.library.TinydbStorage import LibraryRecordTinyDB
    from tinydb.database import Document
    import random

    class LibraryFactory(list):
        doc_id = 1
        letters = ''.join([chr(ord('A')+_) for _ in range(26)])

        @property
        def idMap(self):
            return {
                doc.doc_id: doc
                for doc in self
            }

        @property
        def records(self):
            return [LibraryRecordTinyDB(rec, self) for rec in self]

        @property
        def albums(self):
            return {_['tags']['album'] for _ in self}

        @property
        def artists(self):
            return {_['tags']['artist'] for _ in self}

        @property
        def genres(self):
            return {_['tags']['genre'] for _ in self}

        def __init__(
            self,
            artists=10,
            albums=50,
            tracks=range(6,15),
            years=range(1970,2020),
        ):
            artist_names = [f"Artist Name {self.letters[_]}" for _ in range(artists)]
            for album in range(albums):
                album       = f"Album Name {random.choice(self.letters)}"
                albumtracks = random.choice(tracks)
                artist      = random.choice(artist_names)
                genre       = f"Genre{random.choice(self.letters)}"
                year        = random.choice(years)

                if album in self.albums:
                    continue

                for track in range(1,albumtracks):
                    title = f"Track Title {random.choice(self.letters)}"

                    record = Document({
                        'mrl': f"file:///path/to/music/{artist.replace(' ', '_')}/{album.replace(' ', '_')}/{track:02d}.{title.replace(' ', '_')}.mp3",
                        'tags': {
                            'album':       album,
                            'albumtracks': albumtracks,
                            'artist':      artist,
                            'genre':       genre,
                            'title':       title,
                            'tracknumber': track,
                            'year':        year,
                        },
                        'meta': {
                            'length': round(random.random() * 10 + 20, 2),
                        },
                    }, self.doc_id)

                    self.append(record)
                    self.doc_id += 1

    return LibraryFactory

@pytest.fixture
def setting(app):
    from mnectar.config import Setting
    def get_setting_cm(key, value):
        class SettingCM:
            setting = Setting(key, default=False)

            def __init__(self, app=None, state=""):
                self.app = app
                self.state = state
            def __enter__(self):
                self.setting = self.state
                return self
            def __exit__(self, *arg):
                del self.setting
        return SettingCM(app, value)
    return get_setting_cm

@pytest.fixture
def logconfig():
    orig_levels = {}

    def set_log_levels(levels={}, default='INFO', root='mnectar'):
        orig_levels[''] = logging.getLogger().level
        orig_levels[root] = logging.getLogger(root).level
        orig_levels.update({_: logging.getLogger(_).level for _ in levels.keys()})

        logging.getLogger().setLevel(default)
        logging.getLogger(root).setLevel(default)

        for key, level in levels.items():
            logging.getLogger(key).setLevel(level)

    yield set_log_levels

    for key, level in orig_levels.items():
        logging.getLogger(key).setLevel(level)

@pytest.fixture
def main(app, qtbot):
    from PyQt6 import QtWidgets
    from PyQt6 import QtCore

    class Main(QtWidgets.QMainWindow):
        def __init__(self, app=None, *args, **kw):
            super().__init__(*args, **kw)

            self.app = app
            self.app.ui = self
            self.app.pyqt = True

            self.setupUi()

        def setupUi(self):
            self.setObjectName("MainWindow")
            self.resize(500, 300)
            self.centralwidget = QtWidgets.QWidget(self)
            self.centralwidget.setObjectName("centralwidget")
            self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
            self.verticalLayout.setObjectName("verticalLayout")
            self.label = QtWidgets.QLabel(self.centralwidget)
            self.label.setAlignment(QtCore.Qt.AlignCenter)
            self.label.setObjectName("label")
            self.verticalLayout.addWidget(self.label)
            self.setCentralWidget(self.centralwidget)
            self.menubar = QtWidgets.QMenuBar(self)
            self.setMenuBar(self.menubar)

    main = Main(app)

    qtbot.addWidget(main)

    yield main


