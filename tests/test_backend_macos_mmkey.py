import time
import logging
import platform
import pytest

AppKit = pytest.importorskip("AppKit")

from mnmacos.macos_key import MacMMKeyPlugin
from mnectar.registry  import Registry

def setup_module(module):
    module.logger = logging.getLogger('mnectar.backend.macos_key')
    module.logger_old_level = logger.level
    module.logger.setLevel('DEBUG3')

def teardown_module(module):
    module.logger.setLevel(module.logger_old_level)

@pytest.fixture
def key_event():
    def event_generator(code, state=AppKit.NSKeyDown, subtype=8):
        class Event:
            def subtype(self):
                return subtype
            def data1(self):
                return code << 16 | int(state) << 8
        return Event()
    return event_generator

key_signals = [(16, 'togglePause'),
               (17, 'playNext'),
               (18, 'playPrev'),
               (19, 'playNext'),
               (20, 'playPrev'),]

@pytest.mark.parametrize("code,signal_str",key_signals)
def test_macos_mmkey_event(app, slot, key_event, code, signal_str):
    mmkey = Registry.Control.MacMMKeyPlugin(app)
    mmkey.enable(9)
    signal  = getattr(app.signal, signal_str)
    event   = key_event(code)
    sigSlot = slot(signal)
    result  = mmkey._tap._handle_event(event)
    sigSlot.wait()
    assert sigSlot.count == 1
    assert result
    mmkey.disable()

def test_macos_mmkey_event_invalid(app, key_event):
    mmkey = Registry.Control.MacMMKeyPlugin(app)
    mmkey.enable(9)
    result = mmkey._tap._handle_event(key_event(5))
    assert not result
    result = mmkey._tap._handle_event(key_event(16, state=1))
    assert not result
    result = mmkey._tap._handle_event(key_event(16, subtype=1))
    assert not result
    mmkey.disable()

def test_macos_mmkey_enable_disable(app, slot):
    # Test registration
    assert MacMMKeyPlugin in Registry.Control.plugins

    # Test creation
    mmkey = MacMMKeyPlugin(app)

    # Test Enable & Disable
    # ... Should be enabled by default if the plugin is loaded at all
    # ... test both double enable and double disable to be certain no problems occur
    if not mmkey.running:
        # Because of test framework problems, the initial enable could fail.
        # ... If this happens, first try again
        # ... then mark the test as xfail if everything still fails
        # ... because any actual problems will be shown in other tests.
        mmkey.enable(9)
        if not mmkey.running:
            pytest.xfail("OK for this to fail if other tests succeed. "
                         "This is simply a test setup timing issue.")
    assert mmkey.running
    mmkey.disable()
    assert not mmkey.running
    mmkey.enable(9)
    assert mmkey.running
    mmkey.enable(9)
    assert mmkey.running
    time.sleep(0.1) # Necessary for 
    assert mmkey._tap.event_loop_ready()

